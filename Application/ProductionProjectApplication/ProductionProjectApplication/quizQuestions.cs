﻿///This class is used for editing quiz questions
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductionProjectApplication
{
    public partial class quizQuestions : Form
    {
        SqlDataAdapter adapter;
        SqlCommandBuilder command;
        DataTable table;
        public quizQuestions()
        {
            InitializeComponent();
        }

        /// <summary>
        /// When page is loaded gets data from Database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quizQuestions_Load(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDBConString"].ConnectionString);
            String cmd = "SELECT * FROM Quiz";
            adapter = new SqlDataAdapter(cmd, connection);
            table = new DataTable();
            adapter.Fill(table);
            dataGridView1.DataSource = table;
        }

        /// <summary>
        /// Updates the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                command = new SqlCommandBuilder(adapter);
                adapter.Update(table);
                MessageBox.Show("Updated");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
