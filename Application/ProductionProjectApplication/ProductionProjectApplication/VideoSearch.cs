﻿///This class is used to search youtube and display out information in the form of buttons
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

using Google.Apis.Services;

using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace ProductionProjectApplication
{
    public partial class VideoSearch : Form
    {
        Button[] videoButtons;
        int numOfVideos = 0;
        SearchListResponse resp;
        string videoDesc = null;
        public VideoSearch()
        {
            InitializeComponent();
            YouTubeService youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = "AIzaSyCDIEGqFw4ASg6Cw0ROPpDnedA3AbnNmkE",
                ApplicationName = this.GetType().ToString()
            });

            SearchResource.ListRequest listRequest = youtubeService.Search.List("snippet");
            listRequest.Q = Variables.elementSearch;
            listRequest.MaxResults = 10;
            listRequest.Type = "video";
            resp = listRequest.Execute();
            DisplayButton();
        }
        /// <summary>
        /// Displays button with the video id as Tag
        /// </summary>
        private void DisplayButton()
        {

            videoButtons = new Button[resp.Items.Count];
            //starting position for 1st Element
            int x = 20;
            int y = 10;
            //Button Size
            int sizeX = 100;
            int sizeY = 20;

            //Loops through and displays each element.
            foreach (SearchResult result in resp.Items)
            {
                if (y > 0)
                {
                    y = 20 + y;
                }
                if (y >= 120)
                {
                    x = x + sizeX + 20;
                    y = 30;
                }
                videoButtons[numOfVideos] = new Button();
                videoButtons[numOfVideos].Location = new Point(x, y);
                videoButtons[numOfVideos].Size = new Size(sizeX, sizeY);
                videoButtons[numOfVideos].Font = new Font("Arial", 9, FontStyle.Regular);
                videoButtons[numOfVideos].Tag = result.Id.VideoId;
                videoButtons[numOfVideos].Text = result.Snippet.Title;
                videoButtons[numOfVideos].Click += new EventHandler(displayVideo_Click);
                videoDesc = result.Snippet.Description;
                this.Controls.Add(videoButtons[numOfVideos]);
                numOfVideos++;
            }
        }
        //Calls up the form to display the video
        private void displayVideo_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            Variables.videoId = btn.Tag.ToString();
            DisplayVideo show = new DisplayVideo();
            show.ShowDialog();
            
        }
    }
}
