﻿///This is the entry point of the application, the code for element and legend buttons was found originally online but is edited heavily.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProductionProjectApplication
{
    public partial class PeriodicTable : Form
    {
        //List for Element and Legend buttons
        List<ElementData> elementsList;
        List<LegendData> legendList;
        //The buttons
        Button[] elementbuttons;
        Button[] legendButtons;
        public PeriodicTable()
        {
            InitializeComponent();
            //The filename where Element and Legend information is
            string fileName = "PeriodicTable.txt";
            //sets the path
            string path = Environment.CurrentDirectory;
            //combines to create file path
            path = Path.Combine(path, fileName);
            //create new list
            elementsList = new List<ElementData>();
            legendList = new List<LegendData>();
            //reading file line by line and adding data into list:
            string type = null;
            //checks to see which item it is
            foreach (string data in ReadingData(path))
            {
                if (data == "ELEMENTS")
                {
                    type = "elements";
                    continue;
                }
                else if (data == "LEGEND")
                {
                    type = "legend";
                    continue;
                }
                if (type == "elements")
                {
                    SeperateElementData(data);
                }
                else if (type == "legend")
                {
                    SeperateLegendData(data); 
                }
            }
            //Creates and Displays elements.
            DisplayElements();
            CreatingLegends();
            //If user is child cannot see the edit quiz questions.
            if(Variables.userType == "Child")
            {
                menuStrip.Visible = false;
            }
            ControlsButtons();
        }
        /// <summary>
        /// Buttons for the Logout and Show results forms
        /// </summary>
        private void ControlsButtons()
        {
            Button btnResults = new Button();
            btnResults.Text = "Show Results";
            btnResults.Location = new Point(900, 600);
            btnResults.Size = new Size(100, 50);
            btnResults.Font = new Font("Arial", 8, FontStyle.Regular);
            btnResults.Click += new EventHandler(showQuizResults_Click);
            this.Controls.Add(btnResults);

            Button btnLogout = new Button();
            btnLogout.Text = "Logout";
            btnLogout.Location = new Point(1000, 600);
            btnLogout.Size = new Size(100, 50);
            btnLogout.Font = new Font("Arial", 8, FontStyle.Regular);
            btnLogout.Click += new EventHandler(btnLogout_Click);
            this.Controls.Add(btnLogout);
        }
        /// <summary>
        /// Button event to call the show Results page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showQuizResults_Click(object sender, EventArgs e)
        {
            ShowQuizResults open = new ShowQuizResults();
            open.ShowDialog();
        }
        /// <summary>
        /// Logout button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Creates and adds button to the button list
        /// </summary>
        private void DisplayElements()
        {
            elementbuttons = new Button[elementsList.Count];
            //starting position for 1st Element
            int x = 20;
            int y = 10;
            //Button Size
            int size = 50;
            //Loops through and displays each element.
            foreach (ElementData el in elementsList)
            {
                if (el.LocationX > 0)
                {
                    x = 20 + size * el.LocationX;
                }
                else
                {
                    x = 20;
                }
                if (el.LocationY > 0)
                {
                    y = 10 + size * el.LocationY;
                }
                elementbuttons[el.ElementNumber - 1] = new Button();
                string btnText = String.Format("{0}\r\n{1}\r\n{2}\r\n{3}", el.ElementNumber, el.Symbol, el.FullName, el.AtomicMass);
                elementbuttons[el.ElementNumber - 1].Location = new Point(x, y);
                elementbuttons[el.ElementNumber - 1].Size = new Size(size, size);
                elementbuttons[el.ElementNumber - 1].Font = new Font("Arial", 5, FontStyle.Regular);
                elementbuttons[el.ElementNumber - 1].Text = btnText;
                elementbuttons[el.ElementNumber - 1].Tag = el.FullName;
                elementbuttons[el.ElementNumber - 1].BackColor = GetButtonColor(el.Color);
                elementbuttons[el.ElementNumber - 1].Click += new EventHandler(ButtonElements_Click);
                this.Controls.Add(elementbuttons[el.ElementNumber - 1]);
            }
        }

        /// <summary>
        /// The event handler for when elements are clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonElements_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            Variables.elementSearch = "Element " + btn.Tag.ToString();
            MessageBox.Show(String.Format("Element {0} was selected.", btn.Tag));
            VideoSearch search = new VideoSearch();
            search.FormClosed += new FormClosedEventHandler(search_FormClosed);
            search.ShowDialog();
        }

        /// <summary>
        /// When form search is closed this event handler is triggered, asks user if they would like to take a quiz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void search_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult result = MessageBox.Show("Would you like to take a quiz on this?", "Quiz", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                Quiz quizForm = new Quiz();
                quizForm.ShowDialog();
            }
        }
        /// <summary>
        /// Code for when legend buttons are clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLegends_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            foreach (Button eleButton in elementbuttons)
            {
                if (btn.Text != "Reset")
                {
                    if (eleButton.BackColor != btn.BackColor)
                    {
                        eleButton.Visible = false;
                    }
                    else
                    {
                        eleButton.Visible = true;
                    }
                }
                else
                {
                    eleButton.Visible = true;
                }
            }
        }
        /// <summary>
        /// Gets data from file and stores to list.
        /// </summary>
        /// <param name="data"></param>
        private void SeperateElementData(string data)
        {
            string[] array = data.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);

            ElementData el = new ElementData();
            el.ElementNumber = int.Parse(array[0]);
            el.Symbol = array[1];
            el.FullName = array[2];
            el.AtomicMass = Convert.ToDecimal(array[3]);
            el.Color = int.Parse(array[4]);
            decimal[] xy = SeperateLocation(array[5]);
            el.LocationX = (int)xy[0];
            el.LocationY = (int)xy[1];
            el.Details = array[6];
            //Add data from the file to the list.
            elementsList.Add(el);
        }
        /// <summary>
        /// Seperates the location found in the file
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private decimal[] SeperateLocation(string str)
        {
            string[] arr = str.Split('x');
            decimal x = decimal.Parse(arr[0]);
            decimal y = decimal.Parse(arr[1]);
            return new decimal[] { x, y };
        }

        /// <summary>
        /// Button colour.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private Color GetButtonColor(int value)
        {
            Color backColor = new Color();
            switch (value)
            {
                case 1:
                    backColor = Color.LimeGreen;
                    break;
                case 2:
                    backColor = Color.LightBlue;
                    break;
                case 3:
                    backColor = Color.Orange;
                    break;
                case 4:
                    backColor = Color.Yellow;
                    break;
                case 5:
                    backColor = Color.ForestGreen;
                    break;
                case 6:
                    backColor = Color.Aquamarine;
                    break;
                case 7:
                    backColor = Color.GreenYellow;
                    break;
                case 8:
                    backColor = Color.Red;
                    break;
                case 9:
                    backColor = Color.Pink;
                    break;
                case 10:
                    backColor = Color.Magenta;
                    break;
                case 11:
                    backColor = Color.FloralWhite;
                    break;
            }
            return backColor;
        }

        /// <summary>
        /// Done when Data is required from the file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private IEnumerable<string> ReadingData(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (!line.StartsWith("##") && line != "")
                    {
                        //yield line to indicate iteration
                        yield return line;
                    }
                }
            }
        }

        /// <summary>
        /// Displays legends.
        /// </summary>
        private void CreatingLegends()
        {
            legendButtons = new Button[legendList.Count];
            //starting position for 1st upper left button:
            int x = 10;
            int y = 0;

            decimal size = 48;
            //"Legend" label
            Label legend = new Label();
            legend.Location = new Point(1030, 30);
            legend.Text = "Legend";
            this.Controls.Add(legend);

            foreach (LegendData le in legendList)
            {
                x = Convert.ToInt32(50 + size * le.LocationX);
                if (le.LocationY > 0)
                {
                    y = Convert.ToInt32(60 + size * le.LocationY);
                }
                else
                {
                    y = 60;
                }
                legendButtons[le.Number - 1] = new Button();
                legendButtons[le.Number - 1].Text = le.GroupName;
                legendButtons[le.Number - 1].Size = new Size(100, 25);
                legendButtons[le.Number - 1].Location = new Point(x, y);
                legendButtons[le.Number - 1].Font = new Font("Arial", 8, FontStyle.Regular);
                legendButtons[le.Number - 1].BackColor = GetButtonColor(le.Number);
                legendButtons[le.Number - 1].Tag = le.GroupName;
                legendButtons[le.Number - 1].Click += new EventHandler(ButtonLegends_Click);
                this.Controls.Add(legendButtons[le.Number - 1]);
            }
        }

        /// <summary>
        /// Seperates the legend data
        /// </summary>
        /// <param name="data"></param>
        private void SeperateLegendData(string data)
        {
            string[] array = data.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            LegendData le = new LegendData();
            le.Number = int.Parse(array[0]);
            le.GroupName = array[1];
            decimal[] xy = SeperateLocation(array[2]);
            le.LocationX = xy[0];
            le.LocationY = xy[1];
            legendList.Add(le);
        }
        /// <summary>
        /// If the quiz quesitons button in menu is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quizToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quizQuestions form = new quizQuestions();
            form.ShowDialog();
        }
    }
}
