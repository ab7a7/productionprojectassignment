﻿///This class is the start point of the application. It has the register and login functionality. Generate and hashing password is found online.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;

namespace ProductionProjectApplication
{
    public partial class Form1 : Form
    {
        //Connection to database
        SqlConnection connection;
        public Form1()
        {
            InitializeComponent();
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDBConString"].ConnectionString);
            connection.Open();
            comboBox1.Items.Add("Child");
            comboBox1.Items.Add("Parent");
        }
        /// <summary>
        /// Code to register a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(txtUserReg.Text, @"^(?=[a-zA-Z])[-\w.]{4,20}([a-zA-Z\d]|(?<![-.])_)$"))
            {
                if (new EmailAddressAttribute().IsValid(txtMailReg.Text))
                {
                    //sends email
                    /*SmtpClient SmtpServer = new SmtpClient("smtp.live.com");
                    var mail = new MailMessage();
                    mail.From = new MailAddress("productionapplication@outlook.com");
                    string apples = txtMailReg.Text;
                    mail.To.Add(apples);
                    mail.Subject = "Test Mail - 1";
                    mail.IsBodyHtml = true;
                    string htmlBody;
                    htmlBody = "<h1>Hi</h1>";
                    mail.Body = htmlBody;
                    SmtpServer.Port = 587;
                    SmtpServer.EnableSsl = true;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("productionapplication@outlook.com", "aPPlication");
            
                    try
                    {
                        SmtpServer.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("1" + ex);
                    }*/
                    String salt = CreateSalt(10);
                    String hashPass = GenerateHash(txtPassReg.Text, salt);
                    if (checkUser())
                    {
                        if (comboBox1.SelectedIndex > -1)
                        {
                            string commandString = "INSERT into users (uname, pword, email, salt, usertype) VALUES (@user, @pass, @email, @salt, @usertype)";
                            insertRecord(commandString, salt, hashPass, txtMailReg.Text, txtUserReg.Text, comboBox1.Text);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Username taken.");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Please enter a valid Email");
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid username.");
            }
        }
        /// <summary>
        /// Generates the salt for password
        /// </summary>
        /// <param name="size">number of letters</param>
        /// <returns></returns>
        public String CreateSalt(int size)
        {
            RNGCryptoServiceProvider salt = new RNGCryptoServiceProvider();
            Byte[] num = new Byte[size];
            salt.GetBytes(num);
            return Convert.ToBase64String(num);
        }
        /// <summary>
        /// Generates hash Requires two values
        /// </summary>
        /// <param name="input">The input i,e the password</param>
        /// <param name="salt">The salt</param>
        /// <returns></returns>
        public String GenerateHash(String input, String salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }
        /// <summary>
        /// Add user record to database
        /// </summary>
        /// <param name="commandString">needed to send string</param>
        /// <param name="salt">Salt generated in code</param>
        /// <param name="pass">The user password which is hashed</param>
        /// <param name="mail">User Email address</param>
        /// <param name="uname">Username</param>
        /// <param name="type">User Type</param>
        public void insertRecord(String commandString, String salt, String pass, String mail, String uname, String type)
        {
            try
            {
                SqlCommand cmdInsert = new SqlCommand(commandString, connection);
                cmdInsert.Parameters.AddWithValue("@user", uname);
                cmdInsert.Parameters.AddWithValue("@pass", pass);
                cmdInsert.Parameters.AddWithValue("@email", mail);
                cmdInsert.Parameters.AddWithValue("@salt", salt);
                cmdInsert.Parameters.AddWithValue("@usertype", type);

                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Successful Registeration, Please Login to continue");
                tabControl1.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Submit button. Done when user clicks Submit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_Click(object sender, EventArgs e)
        {
            //checks if Username or Password is empty
            if (string.IsNullOrEmpty(txtUser.Text) || string.IsNullOrEmpty(txtPass.Text))
            {
                MessageBox.Show("Blank entry on username or password.");
            }
            else
            {
                //Get from database where username matches up
                String selcmd = "SELECT * FROM users WHERE uname = @name";
                String uname = txtUser.Text;
                SqlCommand mySqlCommand = new SqlCommand(selcmd, connection);
                mySqlCommand.Parameters.AddWithValue("@name", uname);
                try
                {
                    SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                    int count = 0;
                    //reads from dataase and gets salt needed to generate hash
                    while (mySqlDataReader.Read())
                    {
                        count += 1;
                        Variables.salt = mySqlDataReader["salt"].ToString();
                    }
                    mySqlDataReader.Close();
                    //generates hash of what theu user enetered
                    String hashPass = GenerateHash(txtPassReg.Text, Variables.salt);
                    //compares from user the database hash with what user entered
                    selcmd = "SELECT * FROM users WHERE uname = @uname AND pword = @pword";
                    //mySqlCommand = new SqlCommand(selcmd, connection);
                    mySqlCommand.Parameters.AddWithValue("@uname",uname);
                    mySqlCommand.Parameters.AddWithValue("@pword", hashPass);
                    mySqlDataReader = mySqlCommand.ExecuteReader();
                    count = 0;
                    while (mySqlDataReader.Read())
                    {
                        count += 1;
                        Variables.uname = mySqlDataReader["uname"].ToString();
                        Variables.uid = int.Parse(mySqlDataReader["Id"].ToString());
                        Variables.userType = mySqlDataReader["usertype"].ToString();
                    }
                    mySqlDataReader.Close();
                    //if no details match
                    if (count == 0)
                    {
                        MessageBox.Show("Please Enter Correct Username and Password.");
                    }
                    else if (count == 1)
                    {
                       //IF SUCCESSFUL
                        PeriodicTable form = new PeriodicTable();
                        form.ShowDialog();
                    }
                    else
                    {
                        //more than 1 user 
                        MessageBox.Show("Duplicate users.");
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        /// <summary>
        /// checks if a user exists
        /// </summary>
        /// <returns></returns>
        private bool checkUser()
        {

            String selcmd = "SELECT * FROM users WHERE uname = @uname";
            String uname = txtUserReg.Text;
            SqlCommand mySqlCommand = new SqlCommand(selcmd, connection);
            mySqlCommand.Parameters.AddWithValue("@uname",uname);
            try
            {
                SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                int count = 0;
                while (mySqlDataReader.Read())
                {
                    count += 1;
                }
                if (count >= 1)
                {
                    return false;
                }
                mySqlDataReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return true;
        }
    }
}
