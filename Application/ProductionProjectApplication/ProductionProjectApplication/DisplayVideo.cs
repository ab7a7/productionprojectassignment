﻿///This class is done to seperate the form and to display the video
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductionProjectApplication
{
    public partial class DisplayVideo : Form
    {
        /// <summary>
        /// Displays video
        /// </summary>
        public DisplayVideo()
        {
            InitializeComponent();
            WebBrowser wbYoutube = new WebBrowser();
            wbYoutube.Url = new Uri("http://www.youtube.com/embed/" + Variables.videoId + "?autoplay=1&controls=1&enablejsapi=1");
            panel1.Controls.Add(wbYoutube);
        }
    }
}
