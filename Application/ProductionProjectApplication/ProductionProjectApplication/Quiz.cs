﻿///This class is used to handle the quiz aspect of the program
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductionProjectApplication
{
    public partial class Quiz : Form
    {
        //Variables
        int intTestQuestionNumber = 0;
        SqlConnection connection;
        List<Questions> QuestionRowList = new List<Questions>();
        string uAnswer = null;
        int correctAns = 0;
        string quizChoice = null;
        Button btnGeneral = new Button();
        Button btnSpecific = new Button();
        public Quiz()
        {
            InitializeComponent();
            //sets backcolour
            this.BackColor = Color.Aquamarine;
        }

        /// <summary>
        /// When quiz is loaded sets the page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quiz_Load(object sender, EventArgs e)
        {
            btnGeneral.Text = "All Elements";
            btnGeneral.Size = new Size(100, 50);
            btnGeneral.BackColor = btnNext.BackColor;
            btnGeneral.Location = new Point(lblQuestion.Left, lblQuestion.Top + 30);
            btnGeneral.Font = new Font("Arial", 10, FontStyle.Regular);
            btnGeneral.Click += new EventHandler(quizChoice_Click);

            btnSpecific.Text = "Single Element";
            btnSpecific.Size = new Size(100, 50);
            btnSpecific.BackColor = btnNext.BackColor;
            btnSpecific.Location = new Point(btnGeneral.Left + 110, btnGeneral.Top);
            btnSpecific.Font = new Font("Arial", 8, FontStyle.Regular);
            btnSpecific.Click += new EventHandler(quizChoice_Click);

            ResetForm();
        }

        /// <summary>
        /// If user wants to do quiz again this code is run
        /// </summary>
        private void ResetForm()
        {
            btnNext.Visible = false;
            panel1.Visible = false;
            btnGeneral.Visible = true;
            btnSpecific.Visible = true;
            lblQuestion.Text = "Select quiz type";
            intTestQuestionNumber = 0;
            QuestionRowList = new List<Questions>();
            correctAns = 0;

            if (!this.Controls.Contains(btnGeneral))
            {
                this.Controls.Add(btnGeneral);
            }
            if (!this.Controls.Contains(btnSpecific))
            {
                this.Controls.Add(btnSpecific);
            }
        }
        /// <summary>
        /// Quiz choice event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quizChoice_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            quizChoice = btn.Text;
            StartQuiz();
        }
        /// <summary>
        /// When the user has made a choice, the quiz gets data from database and randomises the order.
        /// </summary>
        private void StartQuiz()
        {
            btnNext.Visible = true;
            panel1.Visible = true;
            btnGeneral.Visible = false;
            btnSpecific.Visible = false;
            try
            {
                int questionNumber = 1;
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDBConString"].ConnectionString);
                connection.Open();
                string[] element = Variables.elementSearch.Split(' ');
                string cmd = null;
                if (quizChoice == "All Elements")
                {
                    cmd = "SELECT TOP(10) * FROM Quiz ORDER BY NEWID()";
                    quizChoice = "General";
                }
                else if (quizChoice == "Single Element") 
                {
                    cmd = "SELECT TOP(10) * FROM Quiz WHERE Element = @element ORDER BY NEWID()";
                    quizChoice = element[1];
                }
                SqlCommand sqlcmd = new SqlCommand(cmd, connection);
                sqlcmd.Parameters.AddWithValue("@element", element[1]);
                SqlDataReader data = sqlcmd.ExecuteReader();
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        Questions question = new Questions();
                        question.intQuestionID = questionNumber;
                        question.strQuestion = (string)data[1];
                        question.strAnswer = (string)data[2];
                        QuestionRowList.Add(question);
                        questionNumber++;
                    }
                }
                else
                {
                    btnNext.Visible = false;
                    panel1.Visible = false;
                    lblQuestion.Font = new Font("Arial", 20, FontStyle.Bold);
                    lblQuestion.ForeColor = Color.Red;
                    lblQuestion.Text = "No Data for quiz";
                }
                data.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }

            if (QuestionRowList.Count >= 1)
            {
                NextQuestion();
            }
        }
        /// <summary>
        /// Displays the next question
        /// </summary>
        private void NextQuestion()
        {
            intTestQuestionNumber++;
            foreach (Questions objQuestion in QuestionRowList)
            {
                if (objQuestion.intQuestionID == intTestQuestionNumber)
                {
                    lblQuestion.Text = objQuestion.strQuestion;
                    //radioButton1.Text = objQuestion.strAnswer;
                    NextAnswer(objQuestion);
                }
            }// end foreach

            ///IF END OF QUIZ
            if (intTestQuestionNumber > QuestionRowList.Count)
            {
                QuizOver();
                
            }
        }

        /// <summary>
        /// Gets Next answer.
        /// </summary>
        /// <param name="objQuestion"></param>
        private void NextAnswer(Questions objQuestion)
        {
            string[] answer = new string[4];
            answer[0] = objQuestion.strAnswer;
            answer[1] = Guid.NewGuid().ToString("n").Substring(0, 2);
            answer[2] = Guid.NewGuid().ToString("n").Substring(0, 2);
            answer[3] = Guid.NewGuid().ToString("n").Substring(0, 2);
            Shuffle(answer);
            radioButton1.Text = answer[0];
            radioButton2.Text = answer[1];
            radioButton3.Text = answer[2];
            radioButton4.Text = answer[3];
        }

        /// <summary>
        /// When quiz ends this is done. Sends the result to database.
        /// </summary>
        private void QuizOver()
        {
            intTestQuestionNumber--;
            double percent = ((double)correctAns / (double)intTestQuestionNumber) * 100;
            lblQuestion.Font = new Font("Arial", 20);
            lblQuestion.Text = "Your result is " + percent + "%";

            string commandString = "INSERT into QuizResults (QuizType, QuizResult, QuizDate, UserId) VALUES (@Qtype, @QResult, @QDate, @UserId)";
            insertRecord(commandString, quizChoice, percent, DateTime.Now, Variables.uid);

            DialogResult again = MessageBox.Show("Would you like to go again?", "Again?", MessageBoxButtons.OKCancel);
            if (again == DialogResult.OK)
            {
                ResetForm();
            }
            else
            {
                panel1.Visible = false;
                btnNext.Visible = false;
            }
        }
        /// <summary>
        /// Inserts record to database
        /// </summary>
        /// <param name="commandString">the string needed for command</param>
        /// <param name="quizType">Type of quiz "General" or "Element"</param>
        /// <param name="score">User score</param>
        /// <param name="quizDate">Date of quiz</param>
        /// <param name="UserId">The user id</param>
        private void insertRecord(string commandString, string quizType, double score, DateTime quizDate, int UserId)
        {
            try
            {
                connection.Open();
                SqlCommand cmdInsert = new SqlCommand(commandString, connection);
                cmdInsert.Parameters.AddWithValue("@Qtype", quizType);
                cmdInsert.Parameters.AddWithValue("@QResult", Math.Round(score,3));
                cmdInsert.Parameters.AddWithValue("@QDate", quizDate);
                cmdInsert.Parameters.AddWithValue("@UserId", UserId);
                cmdInsert.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// Gets the button clicked and sets the answer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                uAnswer = ((RadioButton)sender).Text;
            }
        }

        /// <summary>
        /// WHen the next button is clicked, checks the answer and increments the score
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, EventArgs e)
        {
            foreach (Questions objQuestion in QuestionRowList)
            {
                if (objQuestion.intQuestionID == intTestQuestionNumber)
                {
                    if (radioButton1.Checked || radioButton2.Checked || radioButton3.Checked || radioButton4.Checked)
                    {
                        if (objQuestion.strAnswer == uAnswer)
                        {
                            correctAns++;
                        }
                        radioButton1.Checked = false;
                        radioButton2.Checked = false;
                        radioButton3.Checked = false;
                        radioButton4.Checked = false;
                        NextQuestion();
                    }
                }
            }// end foreach
        }

        /// <summary>
        /// Shuffle the array.
        /// </summary>
        /// <typeparam name="T">Array element type.</typeparam>
        /// <param name="array">Array to shuffle.</param>
        static void Shuffle<T>(T[] array)
        {
            Random rnd = new Random();
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                // NextDouble returns a random number between 0 and 1.
                int r = i + (int)(rnd.NextDouble() * (n - i));
                T t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }
    }
}
