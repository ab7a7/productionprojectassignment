﻿///This class is done to handle the data needed between the different forms.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionProjectApplication
{
    public class Variables
    {
        public static string uname = "";
        private string Username
        {
            get { return uname; }
            set { uname = value; }
        }

        public static int uid;
        private int Userid
        {
            get { return uid; }
            set { uid = value; }
        }

        public static string salt;
        private string Salt
        {
            get { return salt; }
            set { salt = value; }
        }

        public static string elementSearch;
        private string ElementSearch
        {
            get { return elementSearch; }
            set { elementSearch = value; }
        }

        public static string videoId;
        private string VideoId
        {
            get { return videoId; }
            set { videoId = value; }
        }

        public static string userType;
        private string UserType
        {
            get { return userType; }
            set { userType = value; }
        }
        /**
        public static string inspector = "";
        private string Inspector
        {
            get { return inspector; }
            set { inspector = value; }
        }*/
    }
}
