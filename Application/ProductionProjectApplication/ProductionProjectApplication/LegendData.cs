﻿///Handles the Legend Buttons. All the code is made
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionProjectApplication
{
    class LegendData
    {
        public int Number { get; set; }
        public string GroupName { get; set; }
        public decimal LocationX { get; set; }
        public decimal LocationY { get; set; }
    }
}
