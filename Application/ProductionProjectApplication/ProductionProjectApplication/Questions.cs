﻿///This class is used to create question objects to get all the questions.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionProjectApplication
{
    class Questions
    {
        public int intQuestionID { get; set; }
        public string strQuestion { get; set; }
        public string strAnswer { get; set; }
    }
}
