﻿///This class is done to handle the quiz results.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Runtime.InteropServices;

namespace ProductionProjectApplication
{
    public partial class ShowQuizResults : Form
    {
        SqlDataAdapter adapter;
        SqlCommandBuilder command;
        System.Data.DataTable table;
        public ShowQuizResults()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Displays quiz results. Gets user data from database where ID matches up.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowQuizResults_Load(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDBConString"].ConnectionString);
            String cmd = "SELECT * FROM QuizResults WHERE UserId = " + Variables.uid;
            adapter = new SqlDataAdapter(cmd, connection);
            table = new System.Data.DataTable();
            adapter.Fill(table);
            dataGridView1.DataSource = table;
            dataGridView1.Columns["UserId"].Visible = false;
            dataGridView1.Columns["QuizId"].Visible = false;
            dataGridView1.ReadOnly = true;
        }

        /// <summary>
        /// Exports to Excel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel files (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.Title = "Export File To";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet = workbook.ActiveSheet;
                    worksheet.Name = "Exported from Results";

                    ///SET VISIBLE HEADERS
                    List<DataGridViewColumn> listVisible = new List<DataGridViewColumn>();
                    foreach (DataGridViewColumn col in dataGridView1.Columns)
                    {
                        if (col.Visible)
                        {
                            listVisible.Add(col);
                        }
                    }

                    //SET HEADERS TO EXCEL
                    for (int i = 1; i < listVisible.Count + 1; i++)
                    {
                        worksheet.Cells[1, i] = listVisible[i - 1].HeaderText;
                    }


                    //get data
                    for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < listVisible.Count; j++)
                        {
                            worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[listVisible[j].Name].Value.ToString();
                            if (j == 3)
                            {
                                worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[listVisible[j].Name].Value;
                            }
                        }
                    }

                    workbook.SaveAs(saveFileDialog.FileName);
                    workbook.Close();
                    Marshal.ReleaseComObject(workbook);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Cancelled");
            }
        }
    }
}