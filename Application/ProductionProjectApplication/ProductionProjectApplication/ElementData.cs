﻿///This class is done to handle the data which is found in the file. Parts of this are online, but i have edited.
///Author: Abrar Ali
///Version: 1
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionProjectApplication
{
    class ElementData
    {
        public int ElementNumber { get; set; }
        public string Symbol { get; set; }
        public string FullName { get; set; }
        public decimal AtomicMass { get; set; }
        public int Color { get; set; }
        public int LocationX { get; set; }
        public int LocationY { get; set; }
        public string Details { get; set; }
    }
}
